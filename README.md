# syncpkt

sync feeds into a [getpocket](https://getpocket.com) account
built in [go](https://golang.org)

## Requirements
- getpocket api key. you can get one [here](https://getpocket.com/developer/apps/).

## Usage
set the needed env variables required before running

```
var err error
var p syncpkt.Pocket
var nyt feeds.NYT

if err = p.AddItems(&nyt); err != nil {
  fmt.Printf("issue while adding items to pocket: %v\n", err)
  return
}

println("done")
```

