package syncpkt

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"testing/iotest"
	"time"

	"github.com/tidwall/gjson"
)

var (
	getpocketkey = os.Getenv("GETPOCKET_API_KEY")
	endpoints    = map[string]string{
		"authenticate": "https://getpocket.com/v3/oauth/request",
		"authorize":    "https://getpocket.com/v3/oauth/authorize",
		"get":          "https://getpocket.com/v3/get",
		"send":         "https://getpocket.com/v3/send",
	}
)

func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

type feed interface {
	GetURLs() (int, []string)
}

// Pocket structure used to implement Pocket functions
type Pocket struct {
	ReqToken    string
	AccessToken string
	Username    string
	Count       int
}

// AddItems sends urls gathered from the feed GetURLs function
func (p Pocket) AddItems(f feed) error {
	var err error
	var urls []string
	var statusCode int

	statusCode, urls = f.GetURLs()
	if statusCode < 200 && statusCode > 299 {
		return fmt.Errorf("can't react feed, status code returned: %d", statusCode)
	}

	var actions []map[string]interface{}
	for i, url := range urls {
		actions = append(actions, map[string]interface{}{
			"action":  "add",
			"url":     url,
			"item_id": i,
		})

		if p.Count != 0 && i >= p.Count {
			break
		}
	}

	if _, err = p.makeAPIRequest(map[string]interface{}{
		"access_token": p.AccessToken,
		"actions":      actions,
	}, endpoints["send"]); err != nil {
		return err
	}

	return nil
}

// SaveAccess saves access tokens used on future getpocket requests
func (p *Pocket) SaveAccess(sleep bool) error {
	var err error
	var f *os.File
	var dat, j []byte

	tokenFile := fmt.Sprintf(".%s.json", getpocketkey)
	if fileExists(tokenFile) {
		if dat, err = ioutil.ReadFile(tokenFile); err != nil {
			return err
		}
		json.Unmarshal(dat, &p)
	}

	err = p.authenticate()
	fmt.Printf("Request token found. Please visit the following URL and authorize this application: https://getpocket.com/auth/authorize?request_token=%s&redirect_uri=about:blank\n", p.ReqToken)

	consoleReader := bufio.NewReader(os.Stdin)
	if sleep {
		time.Sleep(3 * time.Second)
	} else {
		fmt.Print("Enter ENTER when done: ")
		for {
			s, err := consoleReader.ReadString('\n')
			if s == "YES" || err == io.EOF {
				break
			}

			if err != nil && err != iotest.ErrTimeout {
				panic("GetLines: " + err.Error())
			}
		}
	}

	if err = p.authorize(); err != nil {
		return err
	}

	println("successfully authorized this application")

	if j, err = json.Marshal(*p); err != nil {
		return err
	}

	if f, err = os.Create(fmt.Sprintf(".%s.json", getpocketkey)); err != nil {
		return err
	}
	defer f.Close()

	w := bufio.NewWriter(f)
	_, err = w.Write(j)
	f.Sync()
	w.Flush()

	return err
}

// ListItems returns a list of items currently saved your getpocket account
func (p *Pocket) ListItems(params map[string]interface{}) (map[string]string, error) {
	URLs := make(map[string]string)
	var err error
	var res string

	params["access_token"] = p.AccessToken
	if res, err = p.makeAPIRequest(params, endpoints["get"]); err != nil {
		return URLs, err
	}

	for k, item := range gjson.Get(res, "list").Map() {
		URLs[k] = item.Get("resolved_url").String()
	}

	return URLs, nil
}

func (p *Pocket) authenticate() error {
	if p.ReqToken != "" {
		return nil
	}

	var res string
	var err error
	params := map[string]interface{}{
		"redirect_uri": "about:blank",
	}
	if res, err = p.makeAPIRequest(params, endpoints["authenticate"]); err != nil {
		return err
	}

	p.ReqToken = gjson.Get(res, "code").String()
	return nil
}

func (p *Pocket) authorize() error {
	if p.AccessToken != "" {
		return nil
	}

	var res string
	var err error
	params := map[string]interface{}{
		"code": p.ReqToken,
	}

	if res, err = p.makeAPIRequest(params, endpoints["authorize"]); err != nil {
		return err
	}

	p.AccessToken = gjson.Get(res, "access_token").String()
	return nil
}

func (p *Pocket) makeAPIRequest(params map[string]interface{}, endpoint string) (string, error) {
	var err error
	var res string
	var body, reqBody []byte
	var req *http.Request

	params["consumer_key"] = getpocketkey
	if reqBody, err = json.Marshal(params); err != nil {
		return res, err
	}

	client := http.Client{}
	if req, err = http.NewRequest("POST", endpoint, bytes.NewBuffer(reqBody)); err != nil {
		return res, err
	}

	req.Header.Set("Content-Type", "application/json; charset=UTF-8")
	req.Header.Set("X-Accept", "application/json")
	resp, err := client.Do(req)

	switch true {
	case err != nil:
		return res, err
	case resp.StatusCode < 200:
	case resp.StatusCode > 299:
		return res, fmt.Errorf("issue with response from endpoint %s - %s", endpoints["authenticate"], resp.Status)
	}

	defer resp.Body.Close()

	if body, err = ioutil.ReadAll(resp.Body); err != nil {
		return res, err
	}

	return string(body), nil
}
