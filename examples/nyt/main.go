package main

import (
	"fmt"

	"gitlab.com/ren-rocks/syncpkt"
	"gitlab.com/ren-rocks/syncpkt/feeds"
)

func main() {
	var err error
	var p syncpkt.Pocket
	var nyt feeds.NYT

	if err = p.AddItems(&nyt); err != nil {
		fmt.Printf("issue while adding items to pocket: %v\n", err)
		return
	}

	println("done")
}
