package syncpkt

import (
	"fmt"
	"os"
	"testing"
)

var p Pocket

func TestSaveAccess(t *testing.T) {
	var err error

	key := os.Getenv("GETPOCKET_API_KEY")
	if key == "" {
		t.Error("API key env variable has not been set. Please set GETPOCKET_API_KEY to continue")
	}

	err = p.SaveAccess(true)
	if err != nil {
		t.Errorf("failed to save access json %v", err)
	}

	tokenFile := fmt.Sprintf(".%s.json", key)
	if !fileExists(tokenFile) {
		t.Errorf("could not find file %s", tokenFile)
	}

	t.Log("OK - TestSaveAccess")
}

func TestListItems(t *testing.T) {
	var urls map[string]string
	var err error

	if urls, err = p.ListItems(map[string]interface{}{}); err != nil {
		t.Error(err)
	}

	if len(urls) <= 0 {
		t.Error("no urls returned from pocket API")
	}

	t.Log("OK - TestListItems")
}

type testFeed struct{}

func (t testFeed) GetURLs() (int, []string) {
	return 200, []string{
		"https://www.wral.com/da-clears-roxboro-police-officer-in-fatal-shooting/19277889/",
	}
}

func TestAddItems(t *testing.T) {
	var err error
	var test testFeed

	if err = p.AddItems(&test); err != nil {
		t.Error(err)
	}

	t.Log("OK - TestAddItems")
}
