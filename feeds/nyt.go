package feeds

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"

	"github.com/tidwall/gjson"
)

var (
	nytkey    = os.Getenv("NYT_API_KEY")
	origin    = "api.nytimes.com"
	endpoints = map[string]string{
		"topstories": "topstories/v2/home.json",
	}
)

// NYT feed structure used on syncpkt interface
type NYT struct {
	endpoint   string
	requestURL string
}

func (n *NYT) createRequestURL(key string) error {
	var err error
	var u *url.URL

	tmp := fmt.Sprintf("https://%s/svc/%s", origin, endpoints[key])
	if u, err = url.Parse(tmp); err != nil {
		return err
	}

	q := u.Query()
	q.Set("api-key", nytkey)
	u.RawQuery = q.Encode()
	n.requestURL = u.String()

	return nil
}

// GetURLs pulls all urls from the topstories NYT feed
func (n *NYT) GetURLs() (int, []string) {
	var statusCode int
	var err error
	var body []byte
	var resp *http.Response
	var urls []string

	if err = n.createRequestURL("topstories"); err != nil {
		println(fmt.Errorf("setup new request url failed: %v", err))
		return statusCode, urls
	}

	if resp, err = http.Get(n.requestURL); err != nil {
		println(fmt.Errorf("issue pulling data from %s: %v", n.requestURL, err))
		return statusCode, urls
	}
	defer resp.Body.Close()

	if body, err = ioutil.ReadAll(resp.Body); err != nil {
		println(fmt.Errorf("issue parsing response %s: %v", n.requestURL, err))
		return resp.StatusCode, urls
	}

	result := gjson.GetBytes(body, "results.#.url")
	for _, url := range result.Array() {
		urls = append(urls, url.String())
	}

	return resp.StatusCode, urls
}
