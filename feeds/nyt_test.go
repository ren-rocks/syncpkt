package feeds

import "testing"

func TestPullURLs(t *testing.T) {
	var n NYT
	statusCode, urls := n.GetURLs()

	if statusCode < 200 || statusCode > 299 || len(urls) <= 0 {
		t.Errorf("failed to get urls from endpoint %s. response returned %d status code", n.endpoint, statusCode)
	}
}
